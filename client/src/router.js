import Vue from 'vue'
import Router from 'vue-router'

// Импортирование страниц
import HomePage from './components/HomePage.vue'
import SigninPage from './components/auth/SigninPage.vue'
import SignupPage from './components/auth/SignupPage.vue'
import ProfilePage from './components/user/ProfilePage.vue'
import Logout from './components/auth/Logout.vue'
import store from './store';

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: HomePage
    },
    {
      path: '/signin',
      component: SigninPage
    },
    {
      path: '/signup',
      component: SignupPage
    },
    {
      path: '/profile',
      component: ProfilePage,
      beforeEnter(to, from, next) {
        
        if (localStorage.getItem('token')) {
          next()
        } else {
          next('/signin')
        }
      }
    },
    {
      path: '/logout', 
      component: Logout
    }
  ]
})

export default router
