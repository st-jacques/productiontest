import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import router from '../router';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: null,
        user: null,
        id: null,
        isAuthenticated: false,
        errorText: null
    },
    getters: {
        GET_TOKEN: state => {
            return state.token;
        },
        GET_USER: state => {
            return state.user;
        },
        GET_ID: state => {
            return state.id;
        },
        IS_AUTHENTICATED: state => {
            return state.isAuthenticated;
        },
        GET_ERROR_TEXT: state => {
            return state.errorText;
        }
    },
    mutations: {
        SET_AUTHDATA: (state, payload) => {
            state.token = payload.token
            state.id = payload.id
        },
        SET_USER: (state, payload) => {
            state.user = {
                name: payload.name,
                lastName: payload.lastName,
                age: payload.age,
                email: payload.email,
                position: payload.position,
                placeOfWork: payload.placeOfWork
            }
        }, 
        DELETE_AUTHDATA: (state) => {
            state.token = null,
            state.id = null
        },
        SET_AUTH: (state, val) => {
            state.isAuthenticated = val
        },
        SET_ERROR_TEXT: (state, payload) => {
            state.errorText = payload;
        }
        
    },
    actions: {
        SIGNIN: async (context, payload) => { 
            await Axios.post('http://192.168.1.43:3000/api/signin', {
                email: payload.email,
                password: payload.password
            })
            .then (response => {
                if (response.data.success === true) {
                    localStorage.setItem('token', response.data.token)
                    localStorage.setItem('id', response.data.id)
                    const user = {
                        token: response.data.token,
                        id: response.data.id
                    };
                    // сохраним во vuex
                    context.commit('SET_ERROR_TEXT', null)
                    context.commit('SET_USER', user)
                    context.commit('SET_AUTH', true)
                    // редирект в профиль
                    router.replace('/profile')
                }
            })
            .catch (e => {
                const errText = e.response.data.message
                context.commit('SET_ERROR_TEXT', e.response.data.message)
            })
        },
        SIGNUP: async (context, payload) => {
            await Axios.post('http://192.168.1.43:3000/api/signup', {
                name: payload.name,
                lastName: payload.lastName,
                age: payload.age,
                email: payload.email,
                password: payload.password,
                placeOfWork: payload.placeOfWork,
                position: payload.position
            })
            .then(response => {
                if (response.data.success === true) {
                    localStorage.setItem('token', response.data.token)
                    localStorage.setItem('id', response.data.id)

                    const user = {
                        token: response.data.token,
                        id: response.data.id
                    }
                    context.commit('SET_ERROR_TEXT', null)
                    context.commit('SET_AUTHDATA', user)
                    router.replace('/profile')
                }
            })
            .catch (e => {
                const errText = e.response.data.message
                context.commit('SET_ERROR_TEXT', e.response.data.message)
            })
        },
        LOGOUT:  (context) => {
            context.commit('DELETE_AUTHDATA')
            localStorage.removeItem('token')
            localStorage.removeItem('id')
            context.commit('SET_AUTH', false)
            router.replace('/signin')
        },
        PROFILE: async (context) => {
            const id = localStorage.getItem('id')            
            await Axios.get('http://192.168.1.43:3000/api/profile/')
                .then(response => {
                    const auth = {
                        token: response.data.token,
                        id: response.data.id
                    }
                    const user = {
                        name: response.data.name,
                        lastName: response.data.lastName,
                        age: response.data.age,
                        email: response.data.email,
                        position: response.data.position,
                        placeOfWork: response.data.placeOfWork
                    }
                    context.commit('SET_AUTHDATA', auth)
                    context.commit('SET_USER', user)
                    return user
                })
                .catch(e => {
                    router.replace('/')
                    
                })
        },
        TRY_AUTO_LOGIN: async (context) => {
            const token = localStorage.getItem('token')
            const id = localStorage.getItem('id')
            if (token && id) {
                const auth = {
                    token: token,
                    id: id
                }
                context.commit('SET_AUTH', true)
                context.commit('SET_AUTHDATA', auth)
            } else {
                context.commit('LOGOUT')
            }
        }
    }
});