const Router = require('express').Router;
const controller = require('../../controllers/signin.controller');

const router = Router();

/**
 * @access Public
 * @method POST
 * @access api/signin
 * @description: Signin for users
 */
router.post('/', controller.signinUser);

module.exports = router;