module.exports = {
    home: require('./home.router'),
    signin: require('./signin.router'),
    signup: require('./signup.router'),
    profile: require('./profile.router')
};