const Router = require('express').Router;
const controller = require('../../controllers/signup.controller');

const router = Router();

/**
 * @access Public
 * @method POST
 * @access api/signup
 * @description: Signup for users
 */
router.post('/', controller.signupUser);

module.exports = router;