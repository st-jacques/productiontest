const Router = require('express').Router;
const controller = require('../../controllers/profile.controller');
const verifyToken = require('../../services/passportjwt').jwtVerify;
const User = require('../../models/User')
const router = Router();

/**
 * @access Private
 * @method GET
 * @access api/profile/
 * @description: User profile page
 */
router.get('/', verifyToken, controller.userProfile);

module.exports = router;