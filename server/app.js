const express =  require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const mongoose = require ('mongoose');
const cors = require('cors');
const path = require('path');

const config = require('./config/config');
const router = require('./routes/api/index.router');

const app = express();

mongoose.connect(config.dbURI, { useNewUrlParser: true })
    .then(() => console.log(`DB connection complete`))
    .catch(e => console.log(`DB connection failed: ${e}`));

// Middleware
app.use(bodyParser.json());
app.use(helmet());
app.use(cors());
app.use('/css', express.static(__dirname+'/../client/dist/css'))
app.use('/js', express.static(__dirname+'/../client/dist/js'))

// Routes
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname+'/../client/dist/index.html'))
})

app.use('/api/signin', router.signin);
app.use('/api/signup', router.signup);
app.use('/api/profile/*', router.profile);

app.listen(config.port, '192.168.1.43' , () => console.log(`Server run on http://${config.host}${config.port}`))