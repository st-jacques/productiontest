const Joi = require('joi');

const userSchema = Joi.object().keys({
        name: Joi.string().max(20).required(),
        lastName: Joi.string().max(20).required(),
        age: Joi.number().integer().min(14).max(100),
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{5,30}$/).required(),
        placeOfWork: Joi.string().max(40),
        position: Joi.string().max(40)
    });

module.exports = userSchema;