const jwt = require('jsonwebtoken');
const config = require('../config/config')

module.exports.getToken = function (user) {
    return jwt.sign({id: user._id}, config.secret, {
        expiresIn: 86400
    });
};