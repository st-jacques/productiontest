const Joi = require('joi');

const userLoginSchema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().max(20).min(8)
    });

module.exports = userLoginSchema;