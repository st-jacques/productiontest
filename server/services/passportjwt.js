const User = require('../models/User');
const secret = require('../config/config').secret;
const jwt = require('jsonwebtoken');

module.exports.jwtVerify = function (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        jwt.verify(req.token, secret, (err, authData) => {
            req.id = authData.id;
            if(err) {
                res.sendStatus(403)
            } else {
                next();
            }
        })
  } else {
    res.sendStatus(403);
  }
}
