module.exports = {
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 3000,
    dbURI: 'mongodb://localhost:27017/fullstack',
    secret: 'fin4$nduDSF**^65__*%^4ekmkkFGSVG++43'
};