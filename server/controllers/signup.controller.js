const Joi = require('joi');

const User = require('../models/User');
const userValidationSchema = require('../services/validationDataSchemaSignup');
const getToken = require('../services/token');

module.exports = {
    signupUser: async function (req, res) {
        console.log(req.body.email);
        const candidate = await User.findOne({email: req.body.email});
        console.log('candidate');

        if (candidate) {
            return res.status(409).json({
                success: false,
                message: 'Такой пользователь уже существует'
            });
        } else {
            try {
                console.log('try block')
                const data = await Joi.validate(req.body, userValidationSchema);
                console.log(data);
                const candidate = new User(data);

                await candidate.save()
                    .then((result, err) => {
                        if (err) {
                            res.json({
                                message: 'Error'
                            });
                        }
                        const token = getToken.getToken(candidate);

                        res.status(201).json({
                            success: true,
                            token: `Bearer ${token}`,
                            id: result._id
                        });
                    })
                    .catch(e => console.log('err in db + ' + e));
            } catch (e) {
                res.json(e);
            }
        }
    }
}