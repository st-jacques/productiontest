const Joi = require('joi');

const User = require('../models/User');
const userValidation = require('../services/validationDataSchemeSignin');
const getToken = require('../services/token');

module.exports = {
    signinUser: async function (req, res) {
        console.log('signin function')
        
            const data = await Joi.validate(req.body, userValidation);
            const user = await User.findOne({email: data.email});
            if (!user) {
                return res.status(404).json({
                    success: false,
                    message: 'Такого пользователя не существует'
                });
            }
            user.comparePassword(data.password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        success: false,
                        message: 'Пароли не совпали.'
                    });
                }
                if (result) {
                    const token = getToken.getToken(user);
                    return res.status(200).json({
                        success: true,
                        token: `Bearer ${token}`,
                        id: user._id
                    });
                } else {
                    return res.status(401).json({
                        success: false,
                        message: 'Неправильный пароль.'
                    });
                }
            })
    }
}
