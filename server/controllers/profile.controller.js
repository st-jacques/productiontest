const User = require('../models/User');

module.exports = {
    userProfile: async function (req, res ) {
        console.log('req.id in profile: ', req.id)
        User.findById(req.id)
            .then (user => {
                res.json(user)
        })
    }        
};
