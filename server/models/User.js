const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/config');

const User = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        default: null
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    placeOfWork: {
        type: String,
        default: 'Не указано'
    },
    position: {
        type: String,
        default: 'Не указано'
    }
});

User.pre('save', function (next) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(this.password, salt)
    this.password = hash;
    return next();
});

User.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) {
            cb(err);
        }

        cb(null, isMatch);
    });
};

User.methods.getEmail = function () {
    return this.email;
};

module.exports = mongoose.model('User', User);